<!doctype html>
<html class="no-js" lang="">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Login</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="<?=base_url()?>assets//favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/roboto.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/font-awesome.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/panel.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/feather.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/animate.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/urban.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/urban.skins.css">
  <!-- endbuild -->

</head>

<body>

  <div class="app layout-fixed-header bg-white usersession">
    <div class="full-height">
      <div class="center-wrapper">
        <div class="center-content">
          <div class="row no-margin">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
              <form role="form" method="post" action="<?=base_url()?>login/authenticate" class="form-layout">
                <div class="text-center mb15">
                  <img src="<?=base_url()?>assets/images/logo-dark.png" />
                </div>
                <p class="text-center mb30">Welcome to Urban. Please sign in to your account</p>
                <?php if ($this->session->flashdata('Logged') == 'false') {?>
                <div class="alert alert-danger">
                Invalid Username / Password!
                </div>
                <?php } ?>
                <div class="form-inputs">
                  <input type="username" class="form-control input-lg" placeholder="Username" name="username" required>
                  <input type="password" class="form-control input-lg" placeholder="Password" name="password" required>
                </div>
                <button class="btn btn-success btn-block btn-lg mb15" type="submit">
                  <span>Sign in</span>
                </button>
                <p>
                </p>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="<?=base_url()?>assets/scripts/extentions/modernizr.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery/dist/jquery.js"></script>
  <script src="<?=base_url()?>assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery.easing/jquery.easing.js"></script>
  <script src="<?=base_url()?>assets/vendor/fastclick/lib/fastclick.js"></script>
  <script src="<?=base_url()?>assets/vendor/onScreen/jquery.onscreen.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="<?=base_url()?>assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/accordion.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/animate.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/link-transition.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/panel-controls.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/preloader.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/toggle.js"></script>
  <script src="<?=base_url()?>assets/scripts/urban-constants.js"></script>
  <script src="<?=base_url()?>assets/scripts/extentions/lib.js"></script>
  <!-- endbuild -->
</body>

</html>
