      <!-- main area -->
      <div class="main-content">
        <div class="panel">
          <div class="panel-heading border">
            <ol class="breadcrumb mb0 no-padding">
              <li>
                <span><?=$htitle?></span>
              </li>
            </ol>
          </div>
          <div class="panel-body">
            <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Username</th>
                  <th>Position</th>
                  <th>Mobile</th>
                  <th>Email</th>
                  <th>Add Date</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($users->result() as $row) { ?>
                <tr>
                  <td><?=$row->id?></td>
                  <td><?=$row->username?></td>
                  <td><?=$row->position?></td>
                  <td><?=$row->mobile?></td>
                  <td><?=$row->email?></td>
                  <td><?=$row->added_date?></td>
                  <td><a href="<?=base_url()?>manager/edit_user/<?=$row->id?>">Edit</a>
                  </td>
                  <td>
                  <!-- <a href="javascript:;" class="delete">Delete</a> -->
                  <button type="button" data="<?=$row->id?>" class="btn btn-default swal-warning-confirm btn-sm btn-block">
                  <span>Delete</span>
                </button>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>

      </div>
      <!-- /main area -->