      <!-- main area -->
      <div class="main-content">
        <div class="panel">
          <div class="panel-heading border">
            <ol class="breadcrumb mb0 no-padding">
              <li>
                <span><?=$htitle?></span>
              </li>
            </ol>
          </div>
          <div class="panel-body">
            <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
              <thead>
                <tr>
                  <th>Organization</th>
                  <th>Add Date</th>
                  <th>Salesman ID</th>
                  <th>Status</th>
                  <th>Notes</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($locations->result() as $row) { ?>
                <tr>
                  <td><?=$row->org_name?></td>
                  <td><?=$row->added_date?></td>
                  <td><?=ucfirst($row->username)?></td>
                  <td><?=$row->locstatus?></td>
                  <td><a href="<?=base_url()?>manager/view_notes/<?=$row->id?>">View Notes</a>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>

      </div>
      <!-- /main area -->