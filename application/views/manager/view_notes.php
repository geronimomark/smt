      <!-- main area -->
      <div class="main-content">
        <?php if ($this->session->flashdata('AddNote') == 'success') {?>
        <div class="alert alert-success">
            Successfully added notes!
        </div>
        <?php } elseif ($this->session->flashdata('AddNote') == 'failed') {?>
        <div class="alert alert-danger">
            Error saving to database!
        </div>
        <?php } ?>
        <div class="panel">
          <div class="panel-heading border">
            <ol class="breadcrumb mb0 no-padding">
              <li>
                <span><?=$htitle?></span>
                <hr />
                <span>Organization Name: <b><?=$location->org_name?></b></span>
                <hr />
                <span>Status: <b><?=$location->locstatus?></b> <button class="btn btn-success" data-toggle="modal" data-target=".bs-modal-sm">Update</button></span>
              </li>
            </ol>
          </div>
          <div class="panel-body">
            <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
              <thead>
                <tr>
                  <th>Note</th>
                  <th>Added By</th>
                  <th>Date time</th>
                  <th>Read Date time</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($notes->result() as $row) { ?>
                <tr>
                  <td><?=$row->text?></td>
                  <td><?=$row->username?></td>
                  <td><?=$row->add_note_date_time?></td>
                  <td><?=$row->date_time_status_change?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>

            <hr />

            <form role="form" class="form-validation" method="post" action='<?=base_url()?>manager/addNotes'>
              <input type="hidden" name="location_id" value="<?=$location->id?>"/>

              <div class="form-group mb25">
                <p>Note</p>
                <div>
                  <textarea class="form-control" rows="3" name="notes" maxlength="200" required><?=$this->session->flashdata('Post')['notes']?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label></label>
                <div>
                  <button class="btn btn-primary mr10">Add New Note</button>
                </div>
              </div>
            </form>

          </div>
        <!-- </div> -->

      </div>
      <!-- /main area -->

  <!-- modal -->

  <div class="modal bs-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Update Location Status</h4>
        </div>
        <div class="modal-body">
          <p>Select the new status of <b><?=$location->org_name?></b> Organization</p>
          <div class="row mb25">
            <div class="col-xs-12">
              <label>Status</label>
              <select class="form-control" id="new_status" required aria-required="true">
                <option value="2" <?=($location->status == "2") ? 'selected' : ''?>>Processing</option>
                <option value="1" <?=($location->status == "1") ? 'selected' : ''?>>Won</option>
                <option value="3" <?=($location->status == "3") ? 'selected' : ''?>>Lost</option>
              </select>
              <input type="hidden" value="<?=$location->id?>" id="location_id"></input>
            </div>
          </div>
        </div>
        <div class="modal-footer no-border">
          <a class="btn btn-default" data-dismiss="modal">Close</a>
          <button type="button" class="btn btn-primary" id="status_send">Send</button>
        </div>
      </div>
    </div>
  </div>

  <!-- / modal -->
