    <!-- main area -->
      <div class="main-content">
        <?php if ($this->session->flashdata('AddLocation') == 'success') {?>
        <div class="alert alert-success">
            Successfully added new location!
        </div>
        <?php } elseif ($this->session->flashdata('AddLocation') == 'failed') {?>
        <div class="alert alert-danger">
            Error saving to database!
        </div>      
        <?php } ?>
        <div class="panel">
          <div class="panel-heading border">
            <?=$htitle?> 
          </div>
          <div class="panel-body">
            <form role="form" class="form-validation" method="post" action='<?=base_url()?>manager/postLocation'>

              <div class="form-group mb25">
                <p>Organization Name</p>
                <div>
                  <input value="<?=$this->session->flashdata('Post')['org_name']?>" type="text" class="form-control" name="org_name" placeholder="e.g. XXX Company" required maxlength="100">
                </div>
              </div>

              <div class="form-group mb25">
                <p>Contact Person</p>
                <div>
                  <input value="<?=$this->session->flashdata('Post')['person']?>" type="text" class="form-control" name="person" placeholder="e.g. Bill Gates" required maxlength="100">
                </div>
              </div>              

              <div class="form-group mb25">
                <p>Mobile Number</p>
                <div>
                  <input value="<?=$this->session->flashdata('Post')['mobile']?>" type="number" min="10" max="99999999999999999999" class="form-control" name="mobile" placeholder="e.g. 9121234567" required>
                </div>
              </div>

              <div class="form-group mb25">
                <p>Phone Number</p>
                <div>
                  <input value="<?=$this->session->flashdata('Post')['phone']?>" type="number" min="10" max="99999999999999999999" class="form-control" name="phone" placeholder="e.g. 9111111" required>
                </div>
              </div>

              <div class="form-group mb25">
                <p>Email Address</p>
                <div>
                  <input value="<?=$this->session->flashdata('Post')['email']?>" type="email" class="form-control" name="email" placeholder="e.g. user@yahoo.com" required>
                </div>
              </div>

              <div class="form-group mb25">
                <p>Latitude</p>
                <div>
                  <input id="lat" name="lat" type="text" class="form-control latlong" maxlength="12" placeholder="+-99.9999999" required>
                </div>
              </div>

              <div class="form-group mb25">
                <p>Longitude</p>
                <div>
                  <input id="long" name="long" type="text" class="form-control latlong" maxlength="12" placeholder="+-999.9999999" required>
                </div>
              </div>

              <div class="form-group mb25">
                <p>Salesman</p>
                <div>
                  <select class="form-control" name='salesman' required>
                      <?php foreach ($salesman->result() as $row) { ?>
                        <option value="<?=$row->id?>" <?php if (isset($this->session->flashdata('Post')['salesman']) && $this->session->flashdata('Post')['salesman'] == $row->id) {echo 'selected';} ?>><?=ucfirst($row->username)?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group mb25">
                <p>Status</p>
                <div>
                  <select class="form-control" name='status' required>
                    <option value="2" <?php if (isset($this->session->flashdata('Post')['status']) && $this->session->flashdata('Post')['status'] == 2) {echo 'selected';} ?>>Processing</option>
                    <option value="1" <?php if (isset($this->session->flashdata('Post')['status']) && $this->session->flashdata('Post')['status'] == 1) {echo 'selected';} ?>>Won</option>
                    <option value="3" <?php if (isset($this->session->flashdata('Post')['status']) && $this->session->flashdata('Post')['status'] == 3) {echo 'selected';} ?>>Lost</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label></label>
                <div>
                  <button class="btn btn-primary mr10">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /main area -->