<script type="text/javascript">
    $(function() {
        $("#status_send").on("click", function(){
            swal('Success!', 'Location status will be updated!', 'success');
            setTimeout(function(){window.location = "<?=base_url()?>manager/updateStatus/" + $("#location_id").val() + "/" + $("#new_status").val();}, 2000); 
        });
    });
</script>