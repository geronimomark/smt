<script>
(function ($) {
  'use strict';

  $('.latlong').on('blur', function(event) {
    var regexPattern = /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/;
    if (!regexPattern.test($(this).val())) {
      $(this).val('');
    }
  });

  $('.swal-warning-confirm').on('click', function () {
  	var id = this.getAttribute('data');
    swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this user\'s data!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false,
      },
      function () {
      	if (id == '<?=$this->session->userdata("ID")?>') {
      		swal('Cancelled', 'You cannot delete your own data!', 'error');
      	} else {
	        swal('Deleted!', 'User will be deleted!', 'success');
	        setTimeout(function(){window.location = "<?=base_url()?>manager/deleteUser/" + id;}, 2000);        	
      	}

      });
  });

})(jQuery);

</script>