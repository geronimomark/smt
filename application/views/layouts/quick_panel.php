  <!-- quick launch panel -->
  <div class="quick-launch-panel">
    <div class="container">
      <div class="quick-launcher-inner">
        <a href="<?=base_url()?>assets/javascript:;" class="close" data-toggle="quick-launch-panel">×</a>
        <div class="css-table-xs">
          <div class="col">
            <a href="<?=base_url()?>assets/app-calendar.html">
              <i class="icon-marquee"></i>
              <span>Calendar</span>
            </a>
          </div>
          <div class="col">
            <a href="<?=base_url()?>assets/app-gallery.html">
              <i class="icon-drop"></i>
              <span>Gallery</span>
            </a>
          </div>
          <div class="col">
            <a href="<?=base_url()?>assets/app-messages.html">
              <i class="icon-mail"></i>
              <span>Messages</span>
            </a>
          </div>
          <div class="col">
            <a href="<?=base_url()?>assets/app-social.html">
              <i class="icon-speech-bubble"></i>
              <span>Social</span>
            </a>
          </div>
          <div class="col">
            <a href="<?=base_url()?>assets/charts-flot.html">
              <i class="icon-pie-graph"></i>
              <span>Analytics</span>
            </a>
          </div>
          <div class="col">
            <a href="<?=base_url()?>assets/javascript:;">
              <i class="icon-esc"></i>
              <span>Documentation</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /quick launch panel -->