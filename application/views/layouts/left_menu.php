      <!-- main navigation -->
      <nav role="navigation">

        <ul class="nav">

          <!-- dashboard -->
          <li>
            <a href="<?=base_url()?>home">
              <i class="fa fa-flask"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <!-- /dashboard -->

          <!-- ui -->
          <?php if ($this->session->userdata('Position') == 1) {?>
          <li>
            <a href="<?=base_url()?>assets/javascript:;">
              <i class="fa fa-toggle-on"></i>
              <span>Manager</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="<?=base_url()?>manager/add_new_user">
                  <span>Add New User</span>
                </a>
              </li>
              <li>
                <a href="<?=base_url()?>manager/view_user">
                  <span>View User</span>
                </a>
              </li>
              <li>
                <a href="<?=base_url()?>manager/add_new_location">
                  <span>Add New Location</span>
                </a>
              </li>
              <li>
                <a href="<?=base_url()?>manager/view_added_locations">
                  <span>View Location</span>
                </a>
              </li>
            </ul>
          </li>
          <?php } ?>

          <?php if ($this->session->userdata('Position') == 2) {?>

          <li>
            <a href="<?=base_url()?>assets/javascript:;">
              <i class="fa fa-toggle-on"></i>
              <span>Salesman</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="<?=base_url()?>salesman/view_added_locations">
                  <span>View Locations</span>
                </a>
              </li>
            </ul>
          </li>
          <?php } ?>
          <!-- /ui -->

          <li>
            <a href="<?=base_url().$this->session->userdata('Profile')?>">
              <i class="fa fa-gear"></i>
              <span>Edit Profile</span>
            </a>
          </li>

          <li>
            <a href="<?=base_url()?>login/logout">
              <i class="fa fa-sign-out"></i>
              <span>Logout</span>
            </a>
          </li>

      </nav>
      <!-- /main navigation -->