      <!-- top header -->
      <header class="header navbar">

        <div class="brand visible-xs">
          <!-- toggle offscreen menu -->
          <div class="toggle-offscreen">
            <a href="<?=base_url()?>assets/#" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <!-- /toggle offscreen menu -->

          <!-- logo -->
          <div class="brand-logo">
            <img src="<?=base_url()?>assets/images/logo-dark.png" height="15" alt="">
          </div>
          <!-- /logo -->

        </div>

        <ul class="nav navbar-nav hidden-xs">
          <li>
            <p class="navbar-text">
              <?=$view['title']?>
            </p>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-right hidden-xs">

          <li>
            <a href="<?=base_url()?>assets/javascript:;" data-toggle="dropdown">
              <img src="<?=base_url()?>assets/images/avatar.jpg" class="header-avatar img-circle ml10" alt="user" title="user">
              <span class="pull-left">Welcome! <?=ucfirst($this->session->userdata('User'));?></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="<?=base_url().$this->session->userdata('Profile')?>">Edit Profile</a>
              </li>
              <li>
                <a href="<?=base_url()?>login/logout">Logout</a>
              </li>
            </ul>

          </li>

        </ul>
      </header>
      <!-- /top header -->