<!doctype html>
<html class="no-js" lang="">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title><?=$view['title'] . ' - ' . $view['htitle']?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="<?=base_url()?>assets/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/sweetalert/lib/sweet-alert.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/roboto.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/font-awesome.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/panel.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/feather.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/animate.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/urban.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/styles/urban.skins.css">
  <!-- endbuild -->

</head>

<body>

	<!-- Load Menu -->
	<?php $this->load->view('layouts/quick_panel'); ?>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- Change logo here -->
        <div class="brand-logo">
          <img src="<?=base_url()?>assets/images/logo.png" height="15" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="<?=base_url()?>assets/javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <!-- Load Menu -->
      <?php $this->load->view('layouts/left_menu'); ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

    	<!-- Load Header -->
      	<?php $this->load->view('layouts/header', $view); ?>

    	<!-- Load MAIN Content -->

  		<?php 
          if (!empty($view['contents'])) {
            $this->load->view($view['contents'], $view);
          }
      ?>
      
    </div>
    <!-- /content panel -->

	<!-- Load Footer -->
  	<?php $this->load->view('layouts/footer'); ?>

	<!-- Load Chat -->
  	<?php $this->load->view('layouts/chat'); ?>


  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="<?=base_url()?>assets/scripts/extentions/modernizr.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery/dist/jquery.js"></script>
  <script src="<?=base_url()?>assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery.easing/jquery.easing.js"></script>
  <script src="<?=base_url()?>assets/vendor/fastclick/lib/fastclick.js"></script>
  <script src="<?=base_url()?>assets/vendor/onScreen/jquery.onscreen.js"></script>
  <script src="<?=base_url()?>assets/vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="<?=base_url()?>assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/accordion.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/animate.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/link-transition.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/panel-controls.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/preloader.js"></script>
  <script src="<?=base_url()?>assets/scripts/ui/toggle.js"></script>
  <script src="<?=base_url()?>assets/scripts/urban-constants.js"></script>
  <script src="<?=base_url()?>assets/scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="<?=base_url()?>assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="<?=base_url()?>assets/scripts/pages/form-validation.js"></script>
  <!-- /initialize page scripts -->

  <!-- page level scripts -->
  <script src="<?=base_url()?>assets/vendor/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
  <!-- /page level scripts -->

  <script src="<?=base_url()?>assets/vendor/sweetalert/lib/sweet-alert.min.js"></script>


  <?php if (in_array($view['contents'], $view['dataTableViews'])) { ?>
  <!-- page level scripts -->
  <script src="<?=base_url()?>assets/vendor/datatables/media/js/jquery.dataTables.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="<?=base_url()?>assets/scripts/extentions/bootstrap-datatables.js"></script>
  <script src="<?=base_url()?>assets/scripts/pages/table-edit.js"></script>
  <!-- /initialize page scripts -->

  <?php } ?>

  <?php 
      if (!empty($view['js'])) {
        $this->load->view($view['js']);
      }
  ?>



</body>

</html>
