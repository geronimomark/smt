    <!-- bottom footer -->
    <footer class="content-footer">

      <nav class="footer-right">
        <ul class="nav">
          <li>
            <a href="<?=base_url()?>assets/javascript:;" class="scroll-up">
              <i class="fa fa-angle-up"></i>
            </a>
          </li>
        </ul>
      </nav>

      <nav class="footer-left">
        <ul class="nav">
          <li>
            <a href="#">Copyright <i class="fa fa-copyright"></i> <span>Urban</span> <?=date("Y")?>. All rights reserved</a>
          </li>
        </ul>
      </nav>

    </footer>
    <!-- /bottom footer -->