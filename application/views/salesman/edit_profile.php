    <!-- main area -->
      <div class="main-content">
        <?php if ($this->session->flashdata('EditUser') == 'success') {?>
        <div class="alert alert-success">
            Successfully updated profile!
        </div>
        <?php } elseif ($this->session->flashdata('EditUser') == 'failed') {?>
        <div class="alert alert-danger">
            Error saving to database!
        </div>
        <?php } elseif ($this->session->flashdata('EditUser') == 'duplicate') {?>
        <div class="alert alert-danger">
            Duplicate record exist! Please register a different username.
        </div>        
        <?php } ?>
        <div class="panel">
          <div class="panel-heading border">
            <?=$htitle?>
          </div>
          <div class="panel-body">
            <form role="form" class="form-validation" method="post" action='<?=base_url()?>salesman/updateUser'>
              <input type="hidden" name="id" value="<?=$user->id?>"/>
              <input type="hidden" name="hposition" value="<?=$user->position_id?>"/>
              <input type="hidden" name="hmobile" value="<?=$user->mobile?>"/>

              <div class="form-group mb25">
                <p>Username</p>
                <div>
                  <input value="<?=$user->username?>" type="text" class="form-control" name="username" placeholder="alphanumeric, minimum of 6, maximum of 20 characters" required minlength="6" maxlength="20">
                </div>
              </div>

              <div class="form-group mb25">
                <p>Password</p>
                <div>
                  <input type="password" class="form-control" name="password" placeholder="alphanumeric, minimum of 6, maximum of 20 characters" required minlength="6" maxlength="20">
                </div>
              </div>

              <div class="form-group mb25">
                <p>Position</p>
                <div>
                  <select class="form-control" name='position' required disabled>
                      <?php foreach ($positions->result() as $row) { ?>
                        <option value="<?=$row->id?>" <?php if (isset($user->position_id) && $user->position_id == $row->id) {echo 'selected';} ?>><?=$row->position?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group mb25">
                <p>Mobile Number</p>
                <div>
                  <input value="<?=$user->mobile?>" type="number" min="10" max="99999999999999999999" class="form-control" name="mobile" placeholder="e.g. 9121234567" required disabled>
                </div>
              </div>

              <div class="form-group mb25">
                <p>Email Address</p>
                <div>
                  <input value="<?=$user->email?>" type="email" class="form-control" name="email" placeholder="e.g. user@yahoo.com" required>
                </div>
              </div>

              <div class="form-group">
                <label></label>
                <div>
                  <button class="btn btn-primary mr10">Update</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /main area -->