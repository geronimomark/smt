      <!-- main area -->
      <div class="main-content">
        <?php if ($this->session->flashdata('AddNote') == 'success') {?>
        <div class="alert alert-success">
            Successfully added notes!
        </div>
        <?php } elseif ($this->session->flashdata('AddNote') == 'failed') {?>
        <div class="alert alert-danger">
            Error saving to database!
        </div>
        <?php } ?>
        <div class="panel">
          <div class="panel-heading border">
            <ol class="breadcrumb mb0 no-padding">
              <li>
                <span><?=$htitle?></span>
                <br />
                <span>Organization Name: <b><?=$location->org_name?></b></span>
              </li>
            </ol>
          </div>
          <div class="panel-body">
            <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
              <thead>
                <tr>
                  <th>Note</th>
                  <th>Added By</th>
                  <th>Date time</th>
                  <th>Read Date time</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($notes->result() as $row) { ?>
                <tr>
                  <td><?=$row->text?></td>
                  <td><?=$row->username?></td>
                  <td><?=$row->add_note_date_time?></td>
                  <td><?=$row->date_time_status_change?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>

            <hr />

            <form role="form" class="form-validation" method="post" action='<?=base_url()?>salesman/addNotes'>
              <input type="hidden" name="location_id" value="<?=$location->id?>"/>

              <div class="form-group mb25">
                <p>Note</p>
                <div>
                  <textarea class="form-control" rows="3" name="notes" maxlength="200" required><?=$this->session->flashdata('Post')['notes']?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label></label>
                <div>
                  <button class="btn btn-primary mr10">Add New Note</button>
                </div>
              </div>
            </form>

          </div>
        <!-- </div> -->

      </div>
      <!-- /main area -->