      <!-- main area -->
      <div class="main-content">
        <div class="panel">
          <div class="panel-heading border">
            <ol class="breadcrumb mb0 no-padding">
              <li>
                <span><?=$htitle?></span>
              </li>
            </ol>
          </div>
          <div class="panel-body">
            <table class="table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
              <thead>
                <tr>
                  <th>Organization</th>
                  <th>Add Date</th>
                  <th>Route Me</th>
                  <th>Status</th>
                  <th>Notes</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($locations->result() as $row) { ?>
                <tr>
                  <td><?=$row->org_name?></td>
                  <td><?=$row->added_date?></td>
                  <?php
                    $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
                    if (stripos($ua,'android') !== false) {
                  ?>
                  <td><a href="geo:?q=<?=$row->lat_long?>">Click</a></td>
                  <?php } else { ?>
                  <td><a href="http://maps.google.com/maps?q=<?=$row->lat_long?>">Click</a></td>
                  <?php } ?>
                  <td><?=$row->locstatus?></td>
                  <td><a href="<?=base_url()?>salesman/view_notes/<?=$row->id?>">View Notes</a>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>

      </div>
      <!-- /main area -->