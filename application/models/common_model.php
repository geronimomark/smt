<?php
 /**
  * common_model.php File
  *
  * PHP version 5
  *
  * This file is for Salesman Management Tool
  *
  * @category SMT
  * @package  SMT
  * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
  * @license  http://smt-rc1 SMT
  * @link     http://smt-rc1
  * @since    1.0
  */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Common_model Class
 *
 * @category SMT
 * @package  SMT
 * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @license  http://smt-rc1 SMT
 * @link     http://smt-rc1
 * @since    1.0
 */
class Common_model extends CI_Model
{

    /**
     * Database table
     *
     * @var string
     */
    public $table;

    /**
     * [__construct description]
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * [getAll description]
     *
     * @param mixed $where SQL Where
     *
     * @return object
     */
    public function getAll($where = false)
    {
        if ($where !== false) {
            $this->db->where($where);
            return $this->db->get($this->table);
        }
        return $this->db->get($this->table);
    }

    /**
     * [getJoin description]
     *
     * @param string $select        SQL Select
     * @param mixed  $joinTable     SQL Join Tables
     * @param mixed  $joinCondition SQL Join Condition
     * @param mixed  $where         SQL Where
     *
     * @return object
     */
    public function getJoin($select, $joinTable, $joinCondition, $where = null)
    {
        $this->db->select($select);
        $this->db->from($this->table);
        if ($where !== null) {
            $this->db->where($where);
        }
        if (is_array($joinTable)) {
            foreach ($joinTable as $key => $value) {
                $this->db->join($joinTable[$key], $joinCondition[$key]);
            }
        } else {
            $this->db->join($joinTable, $joinCondition);
        }
        return $this->db->get();
    }

    /**
     * [insert description]
     *
     * @param array $data SQL Values
     *
     * @return object
     */
    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    /**
     * [update description]
     *
     * @param array $data  SQL Values
     * @param mixed $where SQL Where
     *
     * @return object
     */
    public function update($data, $where)
    {
        return $this->db->update($this->table, $data, $where);
    }

    /**
     * [delete description]
     *
     * @param int $id Row ID
     *
     * @return object
     */
    public function delete($id)
    {
        return $this->db->delete($this->table, array('id' => $id));
    }
}
