<?php
 /**
  * locations_model.php File
  *
  * PHP version 5
  *
  * This file is for Salesman Management Tool
  *
  * @category SMT
  * @package  SMT
  * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
  * @license  http://smt-rc1 SMT
  * @link     http://smt-rc1
  * @since    1.0
  */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'common_model.php';

/**
 * Locations_model Class
 *
 * @category SMT
 * @package  SMT
 * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @license  http://smt-rc1 SMT
 * @link     http://smt-rc1
 * @since    1.0
 */
class Locations_model extends Common_model
{

    /**
     * [__construct description]
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->table = 'locations';
    }
}
