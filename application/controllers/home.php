<?php
 /**
  * home.php File
  *
  * PHP version 5
  *
  * This file is for Salesman Management Tool
  *
  * @category SMT
  * @package  SMT
  * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
  * @license  http://smt-rc1 SMT
  * @link     http://smt-rc1
  * @since    1.0
  */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Home Class
 *
 * @category SMT
 * @package  SMT
 * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @license  http://smt-rc1 SMT
 * @link     http://smt-rc1
 * @since    1.0
 */
class Home extends CI_Controller
{
    /**
     * [$data description]
     *
     * @var array
     */
    private $data;

    /**
     * [__construct description]
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('Login') !== 'true') {
            redirect(base_url().'login/logout');
        }

        $this->data['view']['dataTableViews'] = array();
        $this->data['view']['contents'] = null;
        $this->data['view']['title'] = 'Home';
        $this->data['view']['htitle'] = 'Dashboard';
    }

    /**
     * [index description]
     *
     * @return void
     */
    public function index()
    {
        $this->load->view('layouts/default', $this->data);
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
