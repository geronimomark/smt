<?php
 /**
  * manager.php File
  *
  * PHP version 5
  *
  * This file is for Salesman Management Tool
  *
  * @category SMT
  * @package  SMT
  * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
  * @license  http://smt-rc1 SMT
  * @link     http://smt-rc1
  * @since    1.0
  */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Manager Class
 *
 * @category SMT
 * @package  SMT
 * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @license  http://smt-rc1 SMT
 * @link     http://smt-rc1
 * @since    1.0
 */
class Manager extends CI_Controller
{
    /**
     * [$data description]
     *
     * @var array
     */
    private $data;

    /**
     * [__construct description]
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('Position') != 1) {
            redirect(base_url().'login/logout');
        }

        $this->data['view']['title'] = 'Manager';

        $this->data['view']['dataTableViews'] = array('manager/view_user', 'manager/view_added_locations', 'manager/view_notes');

        $this->load->model('positions_model');
        $this->data['view']['positions'] = $this->positions_model->getAll();
    }

    /**
     * [index description]
     *
     * @return void
     */
    public function index()
    {
        redirect(base_url().'manager/add_new_user');
    }

    /**
     * [add_new_user description]
     *
     * @return void
     */
    public function add_new_user()
    {
        $this->data['view']['htitle'] = 'Add New User';
        $this->data['view']['contents'] = 'manager/add_new_user';
        $this->load->view('layouts/default', $this->data);
    }

    /**
     * [edit_user description]
     *
     * @param int $id User ID
     *
     * @return void
     */
    public function edit_user($id)
    {
        $this->data['view']['htitle'] = 'Edit User';
        $this->data['view']['contents'] = 'manager/edit_user';

        $this->load->model('users_model');
        $this->data['view']['user'] = $this->users_model->getAll(array("id" => $id));
        if ($this->data['view']['user']->num_rows() == 1) {
            $this->data['view']['user'] = $this->data['view']['user']->result()[0];
        }
        $this->load->view('layouts/default', $this->data);
    }

    /**
     * [view_user description]
     *
     * @return void
     */
    public function view_user()
    {
        $this->data['view']['htitle'] = 'View User';
        $this->load->model('users_model');
        $select = "users.*, user_positions.position";
        $joinTable = "user_positions";
        $joinCondition = "users.position_id = user_positions.id";
        $this->data['view']['users'] = $this->users_model->getJoin($select, $joinTable, $joinCondition);
        $this->data['view']['js'] = 'manager/js';
        $this->data['view']['contents'] = 'manager/view_user';
        $this->load->view('layouts/default', $this->data);
    }

    /**
     * [add_new_location description]
     *
     * @return void
     */
    public function add_new_location()
    {
        $this->data['view']['htitle'] = 'Add New Location';
        $this->load->model('users_model');
        $this->data['view']['salesman'] = $this->users_model->getAll(array("position_id"=>2));
        $this->data['view']['contents'] = 'manager/add_new_location';
        $this->data['view']['js'] = 'manager/js';
        $this->load->view('layouts/default', $this->data);
    }

    /**
     * [view_added_locations description]
     *
     * @return void
     */
    public function view_added_locations()
    {
        $this->data['view']['htitle'] = 'View Location';

        $this->load->model('locations_model');
        $select = "locations.*, users.username, location_status.status AS locstatus";
        $joinTable[0] = "users";
        $joinCondition[0] = "users.id = locations.salesman_id";
        $joinTable[1] = "location_status";
        $joinCondition[1] = "locations.status = location_status.id";
        $this->data['view']['locations'] = $this->locations_model->getJoin($select, $joinTable, $joinCondition);

        $this->data['view']['contents'] = 'manager/view_added_locations';
        $this->load->view('layouts/default', $this->data);
    }

    /**
     * [addNotes description]
     *
     * @return void
     */
    public function addNotes()
    {
        if ($this->input->post()) {
            $this->load->model('notes_model');
            $data = array(
                'location_id'           => $this->input->post('location_id'),
                'add_note_date_time'    => date('Y-m-d H:i:s'),
                'text'                  => $this->input->post('notes'),
                'added_by'              => $this->session->userdata('ID'),
                'status'                => 'not_read'
            );
            if ($this->notes_model->insert($data)) {
                $this->session->set_flashdata('AddNote', 'success');
            } else {
                $this->session->set_flashdata('Post', $this->input->post());
                $this->session->set_flashdata('AddNote', 'failed');
            }
        } else {
            $this->session->set_flashdata('AddNote', 'failed');
        }
        redirect(base_url().'manager/view_notes/'.$this->input->post('location_id'));
    }    

    /**
     * [view_notes description]
     *
     * @param int $id Location ID
     *
     * @return void
     */
    public function view_notes($id)
    {
        $this->data['view']['htitle'] = 'View Notes';

        $this->load->model('notes_model');
        $select = "notes.*, users.username";
        $joinTable = "users";
        $joinCondition = "users.id = notes.added_by";
        $where = array('location_id' => $id);
        $this->data['view']['notes'] = $this->notes_model->getJoin($select, $joinTable, $joinCondition, $where);

        $this->load->model('locations_model');
        $select = "locations.*, location_status.status AS locstatus";
        $joinTable = "location_status";
        $joinCondition = "locations.status = location_status.id";
        $where = array("locations.id"=>$id);
        $this->data['view']['location'] = $this->locations_model->getJoin($select, $joinTable, $joinCondition, $where)->result()[0];


        $this->data['view']['js'] = 'manager/view_notes_js';
        $this->data['view']['contents'] = 'manager/view_notes';
        $this->load->view('layouts/default', $this->data);        
    }

    /**
     * [edit_profile description]
     *
     * @return void
     */
    public function edit_profile()
    {
        $this->data['view']['htitle'] = 'Edit Profile';
        $this->data['view']['contents'] = 'manager/edit_profile';

        $this->load->model('users_model');
        $this->data['view']['user'] = $this->users_model->getAll(array("id" => $this->session->userdata('ID')))->result()[0];

        $this->load->model('positions_model');
        $this->data['view']['positions'] = $this->positions_model->getAll();

        $this->load->view('layouts/default', $this->data);
    }

    /**
     * [postUser description]
     *
     * @return void
     */
    public function postUser()
    {
        if ($this->input->post()) {
            $this->load->model('users_model');
            if ($this->countDuplicateUser() > 0) {
                $this->session->set_flashdata('Post', $this->input->post());
                $this->session->set_flashdata('AddUser', 'duplicate');
            } else {
                $data = array(
                    'username'      => $this->input->post('username'),
                    'password'      => md5($this->input->post('password')),
                    'position_id'   => $this->input->post('position'),
                    'mobile'        => $this->input->post('mobile'),
                    'email'         => $this->input->post('email'),
                    'added_date'    => date('Y-m-d')
                );
                if ($this->users_model->insert($data)) {
                    $this->session->set_flashdata('AddUser', 'success');
                } else {
                    $this->session->set_flashdata('Post', $this->input->post());
                    $this->session->set_flashdata('AddUser', 'failed');
                }
            }
        } else {
            $this->session->set_flashdata('Post', $this->input->post());
            $this->session->set_flashdata('AddUser', 'failed');
        }
        redirect(base_url().'manager/add_new_user');
    }

    /**
     * [countDuplicateUser description]
     *
     * @return int
     */
    private function countDuplicateUser()
    {
        $where = array('username' => $this->input->post('username'));
        $duplicate = $this->users_model->getAll($where);
        return $duplicate->num_rows();
    }

    /**
     * [updateUser description]
     *
     * @return void
     */
    public function updateUser()
    {
        if ($this->input->post()) {
            $this->load->model('users_model');
            if ($this->countDuplicateUser() > 0) {
                $this->session->set_flashdata('EditUser', 'duplicate');
            } else {
                $id = array("id" => $this->input->post('id'));

                $data = array(
                    'username'      => $this->input->post('username'),
                    'password'      => md5($this->input->post('password')),
                    'position_id'   => $this->input->post('position'),
                    'mobile'        => $this->input->post('mobile'),
                    'email'         => $this->input->post('email'),
                    'added_date'    => date('Y-m-d')
                );
                if ($this->users_model->update($data, $id)) {
                    $this->session->set_flashdata('EditUser', 'success');
                } else {
                    $this->session->set_flashdata('EditUser', 'failed');
                }
            }
        } else {
            $this->session->set_flashdata('EditUser', 'failed');
        }
        redirect(base_url().'manager/edit_user/'.$this->input->post("id"));
    }

    /**
     * [updateUserProfile description]
     *
     * @return void
     */
    public function updateUserProfile()
    {
        if ($this->input->post()) {
            $this->load->model('users_model');
            if ($this->countDuplicateUser() > 1) {
                $this->session->set_flashdata('EditUser', 'duplicate');
            } else {
                $id = array("id" => $this->input->post('id'));

                $data = array(
                    'username'      => $this->input->post('username'),
                    'password'      => md5($this->input->post('password')),
                    'position_id'   => $this->input->post('hposition'),
                    'mobile'        => $this->input->post('hmobile'),
                    'email'         => $this->input->post('email'),
                    'added_date'    => date('Y-m-d')
                );
                if ($this->users_model->update($data, $id)) {
                    $this->session->set_flashdata('EditUser', 'success');
                    $this->session->set_userdata("User", $this->input->post('username'));
                } else {
                    $this->session->set_flashdata('EditUser', 'failed');
                }
            }
        } else {
            $this->session->set_flashdata('EditUser', 'failed');
        }
        redirect(base_url().'manager/edit_profile');
    }

    /**
     * [postLocation description]
     *
     * @return void
     */
    public function postLocation()
    {
        if ($this->input->post()) {
            $this->load->model('locations_model');
            $lat  = str_replace("+", "", $this->input->post('lat'));
            $long = str_replace("+", "", $this->input->post('long'));
            $data = array(
                'org_name'      => $this->input->post('org_name'),
                'person_name'   => $this->input->post('person'),
                'mobile'        => $this->input->post('mobile'),
                'phone'         => $this->input->post('phone'),
                'email'         => $this->input->post('email'),
                'lat_long'      => $lat . ', ' . $long,
                'salesman_id'   => $this->input->post('salesman'),
                'status'        => $this->input->post('status'),
                'added_date'    => date('Y-m-d')
            );
            if ($this->locations_model->insert($data)) {
                $this->session->set_flashdata('AddLocation', 'success');
            } else {
                $this->session->set_flashdata('Post', $this->input->post());
                $this->session->set_flashdata('AddUser', 'failed');
            }
        } else {
            $this->session->set_flashdata('Post', $this->input->post());
            $this->session->set_flashdata('AddLocation', 'failed');
        }
        redirect(base_url().'manager/add_new_location');
    }

    /**
     * [deleteUser description]
     *
     * @param int $id User ID
     *
     * @return [type]     [description]
     */
    public function deleteUser($id)
    {
        $this->load->model('users_model');
        if ($this->users_model->delete($id)) {
                $this->session->set_flashdata('Delete', 'success');
        } else {
            $this->session->set_flashdata('Delete', 'failed');
        }
        redirect(base_url().'manager/view_user');
    }

    /**
     * [updateStatus description]
     *
     * @param int $id     Location ID
     * @param int $status Location Status ID
     *
     * @return void
     */
    public function updateStatus($id, $status)
    {
        $this->load->model('locations_model');
        $data = array("status" => $status);
        $where = array("id" => $id);
        if ($this->locations_model->update($data, $where)) {
            $this->session->set_flashdata('StatusUpdate', 'success');
        } else {
            $this->session->set_flashdata('StatusUpdate', 'failed');
        }
        redirect(base_url().'manager/view_notes/'.$id);
    }
}

/* End of file manager.php */
/* Location: ./application/controllers/manager.php */
