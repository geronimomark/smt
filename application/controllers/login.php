<?php
 /**
  * login.php File
  *
  * PHP version 5
  *
  * This file is for Salesman Management Tool
  *
  * @category SMT
  * @package  SMT
  * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
  * @license  http://smt-rc1 SMT
  * @link     http://smt-rc1
  * @since    1.0
  */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Login Class
 *
 * @category SMT
 * @package  SMT
 * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @license  http://smt-rc1 SMT
 * @link     http://smt-rc1
 * @since    1.0
 */
class Login extends CI_Controller
{
    /**
     * [$data description]
     *
     * @var array
     */
    private $data;

    /**
     * [__construct description]
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->data['view']['title'] = 'Login';

        $this->load->model('users_model');
    }

    /**
     * [index description]
     *
     * @return void
     */
    public function index()
    {
        $this->load->view('login/index');
    }

    /**
     * [logout description]
     *
     * @return void
     */
    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url().'login');
    }

    /**
     * [authenticate description]
     *
     * @return void
     */
    public function authenticate()
    {
        if ($this->input->post()) {
            $param = array(
                "username" => $this->input->post('username'),
                "password" => md5($this->input->post('password'))
            );
            $result = $this->users_model->getAll($param);
            if ($result->num_rows() > 0) {
                $session = array(
                    "Login"     => "true",
                    "User"      => $this->input->post('username'),
                    "ID"        => $result->result_object()[0]->id,
                    "Position"  => $result->result_object()[0]->position_id,
                    "Profile"   => ($result->result_object()[0]->position_id == 1) ? "manager/edit_profile" : "salesman/edit_profile"
                );
                $this->session->set_userdata($session);
                $this->session->set_flashdata('Logged', 'true');
                redirect(base_url().'home');
            } else {
                $this->session->set_userdata('Login', 'false');
                $this->session->set_flashdata('LoginPost', $this->input->post());
                $this->session->set_flashdata('Logged', 'false');
            }
        } else {
            $this->session->set_flashdata('LoginPost', $this->input->post());
            $this->session->set_flashdata('Logged', 'false');
        }
        redirect(base_url().'login');
    }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
