<?php
 /**
  * salesman.php File
  *
  * PHP version 5
  *
  * This file is for Salesman Management Tool
  *
  * @category SMT
  * @package  SMT
  * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
  * @license  http://smt-rc1 SMT
  * @link     http://smt-rc1
  * @since    1.0
  */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Salesman Class
 *
 * @category SMT
 * @package  SMT
 * @author   Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @license  http://smt-rc1 SMT
 * @link     http://smt-rc1
 * @since    1.0
 */
class Salesman extends CI_Controller
{
    /**
     * [$data description]
     *
     * @var array
     */
    private $data;

    /**
     * [__construct description]
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('Login') !== 'true') {
            redirect(base_url().'login/logout');
        }

        $this->data['view']['dataTableViews'] = array('salesman/view_added_locations', 'salesman/view_notes');

        $this->data['view']['title'] = 'Salesman';
    }

    /**
     * [index description]
     *
     * @return void
     */
    public function index()
    {
        redirect(base_url().'salesman/view_added_locations');
    }

    /**
     * [view_added_locations description]
     *
     * @return void
     */
    public function view_added_locations()
    {
        $this->data['view']['htitle'] = 'View Location';

        $this->load->model('locations_model');
        $select = "locations.*, location_status.status AS locstatus";
        $joinTable = "location_status";
        $joinCondition = "locations.status = location_status.id";
        $where = array("salesman_id" => $this->session->userdata('ID'));
        $this->data['view']['locations'] = $this->locations_model->getJoin($select, $joinTable, $joinCondition, $where);

        $this->data['view']['contents'] = 'salesman/view_added_locations';
        $this->load->view('layouts/default', $this->data);
    }

    /**
     * [view_notes description]
     *
     * @param int $id Location ID
     *
     * @return void
     */
    public function view_notes($id)
    {
        $this->data['view']['htitle'] = 'View Notes';

        $this->load->model('notes_model');
        $select = "notes.*, users.username";
        $joinTable = "users";
        $joinCondition = "users.id = notes.added_by";
        $where = array('location_id' => $id);
        $this->data['view']['notes'] = $this->notes_model->getJoin($select, $joinTable, $joinCondition, $where);

        $this->load->model('locations_model');
        $this->data['view']['location'] = $this->locations_model->getAll(array("id"=>$id))->result()[0];
        $this->data['view']['contents'] = 'salesman/view_notes';
        $this->load->view('layouts/default', $this->data);
    }

    /**
     * [addNotes description]
     *
     * @return void
     */
    public function addNotes()
    {
        if ($this->input->post()) {
            $this->load->model('notes_model');
            $data = array(
                'location_id'           => $this->input->post('location_id'),
                'add_note_date_time'    => date('Y-m-d H:i:s'),
                'text'                  => $this->input->post('notes'),
                'added_by'              => $this->session->userdata('ID'),
                'status'                => 'not_read'
            );
            if ($this->notes_model->insert($data)) {
                $this->session->set_flashdata('AddNote', 'success');
            } else {
                $this->session->set_flashdata('Post', $this->input->post());
                $this->session->set_flashdata('AddNote', 'failed');
            }
        } else {
            $this->session->set_flashdata('AddNote', 'failed');
        }
        redirect(base_url().'salesman/view_notes/'.$this->input->post('location_id'));
    }

    /**
     * [edit_profile description]
     *
     * @return void
     */
    public function edit_profile()
    {
        $this->data['view']['htitle'] = 'Edit Profile';
        $this->data['view']['contents'] = 'salesman/edit_profile';

        $this->load->model('users_model');
        $this->data['view']['user'] = $this->users_model->getAll(array("id" => $this->session->userdata('ID')))->result()[0];

        $this->load->model('positions_model');
        $this->data['view']['positions'] = $this->positions_model->getAll();

        $this->load->view('layouts/default', $this->data);
    }

    /**
     * [countDuplicateUser description]
     *
     * @return int
     */
    private function countDuplicateUser()
    {
        $where = array('username' => $this->input->post('username'));
        $duplicate = $this->users_model->getAll($where);
        return $duplicate->num_rows();
    }

    /**
     * [updateUser description]
     *
     * @return void
     */
    public function updateUser()
    {
        if ($this->input->post()) {
            $this->load->model('users_model');
            if ($this->countDuplicateUser() > 1) {
                $this->session->set_flashdata('EditUser', 'duplicate');
            } else {
                $id = array("id" => $this->input->post('id'));

                $data = array(
                    'username'      => $this->input->post('username'),
                    'password'      => md5($this->input->post('password')),
                    'position_id'   => $this->input->post('hposition'),
                    'mobile'        => $this->input->post('hmobile'),
                    'email'         => $this->input->post('email'),
                    'added_date'    => date('Y-m-d')
                );
                if ($this->users_model->update($data, $id)) {
                    $this->session->set_flashdata('EditUser', 'success');
                    $this->session->set_userdata("User", $this->input->post('username'));
                } else {
                    $this->session->set_flashdata('EditUser', 'failed');
                }
            }
        } else {
            $this->session->set_flashdata('EditUser', 'failed');
        }
        redirect(base_url().'salesman/edit_profile');
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
