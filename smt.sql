-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2016 at 04:14 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smt`
--
CREATE DATABASE IF NOT EXISTS `smt` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `smt`;

-- --------------------------------------------------------

--
-- Table structure for table `location_status`
--

CREATE TABLE `location_status` (
  `id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `location_status`
--

INSERT INTO `location_status` (`id`, `status`) VALUES
(1, 'Won'),
(2, 'Processing'),
(3, 'Lost');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `org_name` varchar(255) NOT NULL,
  `person_name` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `lat_long` varchar(100) NOT NULL,
  `salesman_id` int(10) NOT NULL,
  `status` int(2) NOT NULL,
  `added_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --
-- -- Dumping data for table `locations`
-- --

-- INSERT INTO `locations` (`id`, `org_name`, `person_name`, `mobile`, `phone`, `email`, `lat_long`, `salesman_id`, `status`, `added_date`) VALUES
-- (1, 'Microsoft', 'Bill Gates', '9111111', '8111111', 'micro@yahoo.com', '12.3232323, 123.2321312', 3, 2, '2016-03-08'),
-- (2, 'Google', 'Google test', '12345678', '123456789', 'goog@go.com', '12.3232323, 123.2321319', 3, 2, '2016-03-08'),
-- (3, 'Facebook', 'Mark Zuckerberg', '123123123', '12312312', 'mark_geronimo@rocketmail.com', '12.3232323, 123.2321312', 7, 1, '2016-03-08'),
-- (4, 'last ', 'last', '12312122', '123123123', 'manager1@yahoo.com', '12.3232323, 123.2321312', 3, 2, '2016-03-08'),
-- (5, 'Freelancer', 'freebee', '123213213', '123123123', 'manager1@yahoo.com', '99.9123221, -542.8102323', 7, 2, '2016-03-08'),
-- (6, 'XXX Co.', 'MarkG', '12312312', '123123123', 'markG@yahoo.com', '-15.1234567, 120.7894523', 14, 2, '2016-03-08');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `add_note_date_time` datetime NOT NULL,
  `text` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `date_time_status_change` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `notes`
--

-- INSERT INTO `notes` (`id`, `location_id`, `add_note_date_time`, `text`, `added_by`, `status`, `date_time_status_change`) VALUES
-- (1, 3, '2016-03-08 00:00:00', 'I did 123', 3, 'not_read', '0000-00-00 00:00:00'),
-- (2, 3, '2016-03-08 00:00:00', 'a', 3, 'not_read', '0000-00-00 00:00:00'),
-- (3, 3, '2016-03-08 15:39:03', 'test again', 3, 'not_read', '0000-00-00 00:00:00'),
-- (4, 2, '2016-03-08 16:02:34', 'testgoogle', 3, 'not_read', '0000-00-00 00:00:00'),
-- (5, 2, '2016-03-08 16:04:25', 'what the f r u doing?', 1, 'not_read', '0000-00-00 00:00:00'),
-- (6, 6, '2016-03-08 16:20:29', 'Work work!', 1, 'not_read', '0000-00-00 00:00:00'),
-- (7, 6, '2016-03-08 16:21:10', 'f U!', 14, 'not_read', '0000-00-00 00:00:00'),
-- (8, 3, '2016-03-09 15:53:02', 'test', 1, 'not_read', '0000-00-00 00:00:00'),
-- (9, 3, '2016-03-09 15:53:12', 'wawawa', 1, 'not_read', '0000-00-00 00:00:00'),
-- (10, 3, '2016-03-09 15:54:48', 'asdsadasasd', 1, 'not_read', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_positions`
--

CREATE TABLE `user_positions` (
  `id` int(11) NOT NULL,
  `position` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `user_positions`
--

INSERT INTO `user_positions` (`id`, `position`) VALUES
(1, 'manager'),
(2, 'salesman');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `position_id` int(1) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `added_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `position_id`, `mobile`, `email`, `added_date`) VALUES
(1, 'manager', '1d0258c2440a8d19e716292b231e3190', 1, '123456789', 'manager@yahoo.com', '2016-03-09');
-- (3, 'salesman10', '7f0495c1452b0c8ca884c84c1ff27346', 2, '912123132', 'salesman1@yahoo.com', '2016-03-08'),
-- (4, 'salesman2', 'ec58e2732caed60d04e8eb5a74a79284', 2, '912123132', 'salesman2@yahoo.com', '2016-03-08'),
-- (5, 'salesman3', '910100c6cc893905862fcad8ffea0951', 2, '123123123', 'manager1@yahoo.com', '2016-03-08'),
-- (7, 'salesman5', '986e4f01789ae5d8e14d22612fa6fc20', 2, '123123123113', 'manager1@yahoo.com', '2016-03-08'),
-- (8, 'manager98', '7a279d81a9d6263033c6ed012beca73d', 1, '1231223', 'manager1@yahoo.com', '2016-03-08'),
-- (9, 'manager97', '8e5b3ea152213435a5d413e96fae7698', 1, '1232113', 'manager1@yahoo.com', '2016-03-08'),
-- (10, 'manager90', '9d0e7fccf5067a1fc8723e041db88a85', 1, '12312123', 'manager1@yahoo.com', '2016-03-08'),
-- (11, 'manager11', '4397e19a0c112054d4c179bd85d6f431', 1, '1231223', 'manager1@yahoo.com', '2016-03-08'),
-- (12, 'manager1', 'c240642ddef994358c96da82c0361a58', 1, '12321213123', 'test@y', '2016-03-08'),
-- (14, 'salesman77', 'a1cfb0660992e09a2093073bce03f166', 2, '123123123213', 'salesman77@yahoo.com', '2016-03-08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `location_status`
--
ALTER TABLE `location_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_positions`
--
ALTER TABLE `user_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `location_status`
--
ALTER TABLE `location_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_positions`
--
ALTER TABLE `user_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
